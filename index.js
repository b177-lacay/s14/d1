// Javascript Comments
// this is a single line comment
/* this is a multi line comments*/

// syntax and Statements

/*
 - Statements in programming are instructions that we tell the computer to perform. It usually ends with semi colon(;)*

 - syntax- set of rules that describes how statements must be constructed*/

 console.log("Hello,World")

 //Variables
 // IT is used to contain data
 // Declaring variable

 /*
 Syntax
 let/const variableName;*/

 let myVariable;

 console.log(myVariable);

 // let hello;

 //  console.log(hello);

 // Initializing Variables
/* syntax
	let/const variableName = value;
*/

let productName='desktop computer';
console.log(productName);

let productPrice= 18999;
console.log(productPrice);

const	interest=3.539;

// Reassigning variable

productName= 'Laptop'
console.log(productName)

// DEclaring a variable before initializing a value
let supplier;
supplier= "John Smith Tradings";
console.log(supplier)

// Multiple variable declarations

let productCode='DC017', productBrand='Dell';
console.log(productCode,productBrand);

// Using a variable with a reserved keyword
/*
const let = "Hello";
console.log(let);
*/

// Data Types

/* Strings
 -series of charac that creat a word,phrase,sentence or anything related to creating text (""))('')
*/

let country = 'Philippines'
let province = "Metro Manila"

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+"

let fullAddress = province + ', ' +country;

console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The esopate character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in bet text (line berak)
let mailAddress = 'Metro Manila\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early"
console.log(message)
message = 'John\'s employees went home early';
console.log(message)

// Numbers 
// integers/Whole numbers
let headcount = 26;
console.log(headcount);

// Decimal number/float
let grade = 98.7;
console.log(grade)

// exponential Notation (e)
let planetDistance =2e10
console.log(planetDistance)

// Combining text and numbers
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of ceratin things

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct " 
	+ inGoodConduct)

// Arrays
// Arrays are a special kind of data type that's use to store multiple values

/* syntax
	 let/const arrayName = [elementA, elementB,...];
*/
	let grades = [98.7,92.1,90.2,94.6];
	console.log(grades)

// Different data types
let details =["John", "Smith", 32,true];
console.log(details);

// Objects
// Objects are another special kind of data type that's udes to mimic real world objcectd/items

/*
syntax
let/const objectName = {
	propertyA: value
	propertyB: value
}
*/

let person={
	fullName:'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact:["+63917 123 4567", "8123 4567"],
	address : {
		houseNumber: '345',
		city: 'Manila'
	}

}
console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}
console.log(myGrades);

// Null variable
// Used intentionally to express the absence of a value in a variable

let spouse= null;
let myString = "";
console.log(spouse);
console.log(myString);

// Funtions
/*  syntax
	function functionName(){
	line/block of code to be executed by the funcion;
	}; 
*/

// function printName(){
// 	console.log("My name is John")
// };

// printName();

// console.log("My name is John")

function printName (name){
	console.log("My name is " + name);
};
printName("John");

function myAnimal(animal){
	console.log("My favorite animal is " + animal)
};

myAnimal("dog");

function argumentFunction(){
	console.log("this function was passed an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
};

invokeFunction(argumentFunction);

// Creating another function with multiple parameters

function createFullName(firstName,middleName,lastName){
	console.log(firstName + ' ' + middleName + ' '+ lastName);
};

createFullName('Juan', 'Diaz', ' Dela Cruz');
createFullName('Juan', ' Dela Cruz');   
createFullName('Juan', 'Diaz', ' Dela Cruz' , 'Hello');   
createFullName('Juan','','Dela Cruz'); 

// Creating a function using "return" statement
// "return" statement allows output of a function to be passed to the line/block of caode that invoked/called the function

function returnFullName(firstName,  middleName, lastName){
return firstName + ' '+ middleName + ' ' + lastName};

console.log(returnFullName('Maria','Clara', 'Ibarra'));

let completeName= returnFullName('Maria','Clara', 'Ibarra');
console.log(completeName);